Black Dragon Viewer
====================

This project manages the source code for the
[Black Dragon](https://niranv-sl.blogspot.de/) Viewer.

This source is available as open source; for details on licensing, see
[the licensing page on the Second Life wiki](https://wiki.secondlife.com/wiki/Linden_Lab_Official:Second_Life_Viewer_Licensing_Program)
